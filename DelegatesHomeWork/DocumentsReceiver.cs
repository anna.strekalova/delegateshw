﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace DelegatesHomeWork
{
    public class DocumentsReceiver : IDisposable
    {
        public event EventHandler DocumentsReady;
        public event EventHandler TimedOut;

        private static FileSystemWatcher _watcher;
        private static Timer _timer;

        private static string _targetDirectory;
        private static uint _waitingInterval;
        private static List<string> _fileNames;
        private static bool _isDocumentReady = false;


        public DocumentsReceiver(List <string> fileNames)
        {
            _fileNames = fileNames;
        }

        public void Start(string targetDirectory, uint waitingInterval)
        {
            if (!Directory.Exists(targetDirectory))
                Directory.CreateDirectory(targetDirectory);

            _targetDirectory = targetDirectory;
            _waitingInterval = waitingInterval;

            SetTimer();
            SetFileWatcher();

            _timer.Start();
        }

        private void SetFileWatcher()
        {
            _watcher = new FileSystemWatcher();
            _watcher.Path = _targetDirectory;
            _watcher.Changed += OnChangedDirectory;
            EnableRaisingEventsFileWatcher(true);
        }

        private void SetTimer()
        {
            _timer = new Timer();
            _timer.Interval = _waitingInterval;
            _timer.Elapsed += OnTimeElapsed;
            _timer.Enabled = true;
        }

        private void OnTimeElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            TimedOut?.Invoke(sender, e);
            EnableRaisingEventsFileWatcher(false);
        }

        private static void EnableRaisingEventsFileWatcher(bool enable)
        {
            _watcher.EnableRaisingEvents = enable;
        }

        private void OnChangedDirectory(object sender, FileSystemEventArgs e)
        {
            if (_isDocumentReady)
                return;

            var files = Directory.GetFiles(_targetDirectory).ToList();
            var result = _fileNames.Where(x => files.Contains(Path.Combine(_targetDirectory, x)));
            if(result != null && result.Count() == _fileNames.Count() && files.Count() == _fileNames.Count())
            {
                DocumentsReady?.Invoke(sender, e);
                _isDocumentReady = true;
                _timer.Stop();
                EnableRaisingEventsFileWatcher(false);
            }
        }

        public void Dispose()
        {
            
            _watcher.Changed -= OnChangedDirectory;
            _watcher.Dispose();

            _timer.Elapsed -= OnTimeElapsed;
            _timer.Dispose();

        }
    }
}
