﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DelegatesHomeWork
{
    class Program
    {
        private static bool _isStopWork = false; 

        static void Main(string[] args)
        {
            List<string> documentsName = new List<string> { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };
            const uint time = 20000;

            StartAcceptingDocuments(documentsName, time);

            Console.ReadKey();
        }

        private static void StartAcceptingDocuments(List<string> documentsName, uint time)
        {
            var targetDirectory = GetTargetDirectory();
            if (string.IsNullOrEmpty(targetDirectory))
                Console.WriteLine("Не указан путь к директории.");
            else
            {
                using (var docReceiver = new DocumentsReceiver(documentsName))
                {

                    docReceiver.DocumentsReady += OnDocumentsReady;
                    docReceiver.TimedOut += OnTimedOut;


                    Console.WriteLine($"Загрузите документы в указанную директорию за время {time} миллисекунд.");
                    docReceiver.Start(targetDirectory, time);


                    while (!_isStopWork)
                    {

                    }
                }
            }
        }

        private static string GetTargetDirectory()
        {
            Console.WriteLine("Укажите путь к директории в которой будет храниться комплект документов для подачи заявления\n");
            return Console.ReadLine();
        }

        private static void OnDocumentsReady(object sender, EventArgs e)
        {
            Console.WriteLine("Все документы готовы.");
            _isStopWork = true;
        }

        private static void OnTimedOut(object sender, EventArgs e)
        {
            Console.WriteLine("Превышено ожидание загрузки докуметов.");
            _isStopWork = true;
        }

    }
}
